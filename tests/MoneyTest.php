<?php

include dirname(__DIR__).'/vendor/autoload.php';
require dirname(__DIR__).'/app/Money.php';

use PHPUnit\Framework\TestCase;

class MoneyTest extends PHPUnit_Framework_TestCase
{
    public function testGet()
    {
        $money = new Money(100);

        $expected = 100;

        $this->assertEquals($expected, $money->getAmount());
    }

    public function testSet()
    {
        $money = new Money(100);

        $money->setAmount(200);
        $this->assertEquals(200, $money->getAmount());
    }
}